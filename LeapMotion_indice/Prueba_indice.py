# Reconocimiento de la posicion de los dedos usando Leap Motion.
# Usa trigonometria para evaluar la posicion en la que se encuentra el dedo actualmente.
# Tambien usa variables globales para determinar las coordendas 0,0,0 cada vez que detecta una mano
# y no tener que basarse en las coordenadas 0,0,0 que proporciona Leap Motion
#
# No usa el eje X en ningun momento del programa porque el dedo robotico no se va a mover en ese eje

import Leap, sys, thread, time, math, time, os, serial
from Leap import CircleGesture, KeyTapGesture, ScreenTapGesture, SwipeGesture

# -- Inicializar conexion con arduino --
arduino = serial.Serial('COM5', 9600, timeout=1)

# -- Variables globales --
index_global = [0,1]
middle_global = [0,1]
ring_global = [0,1]
pinky_global = [0,1]

# -- Grados de los dedos --
index_grados = 0
middle_grados = 0
ring_grados = 0
pinky_grados = 0

class SampleListener(Leap.Listener):
    finger_names = ['Thumb', 'Index', 'Middle', 'Ring', 'Pinky']
    bone_names = ['Metacarpal', 'Proximal', 'Intermediate', 'Distal']
    state_names = ['STATE_INVALID', 'STATE_START', 'STATE_UPDATE', 'STATE_END']

    def on_init(self, controller):
        print "Initialized"

    def on_connect(self, controller):
        print "Connected"
        # Enable gestures
        controller.enable_gesture(Leap.Gesture.TYPE_CIRCLE);
        controller.enable_gesture(Leap.Gesture.TYPE_KEY_TAP);
        controller.enable_gesture(Leap.Gesture.TYPE_SCREEN_TAP);
        controller.enable_gesture(Leap.Gesture.TYPE_SWIPE);
    def on_disconnect(self, controller):
        print "Disconnected"

    def on_exit(self, controller):
        print "Exited"

    def on_frame(self, controller):
        global index_grados
        # Usar el frame mas reciente
        frame = controller.frame()

        print "Frame id: %d, timestamp: %d, hands: %d, fingers: %d, tools: %d, gestures: %d" % (frame.id, frame.timestamp, len(frame.hands), len(frame.fingers), len(frame.tools), len(frame.gestures()))

        # -- Reinicia la posicion de la mano si no la encuentra --
        if len(frame.hands) == 0:
            index_global[0] = 0 # eje y
            index_global[1] = 1 # eje z
            middle_global[0] = 0 # eje y
            middle_global[1] = 1 # eje z
            ring_global[0] = 0 # eje y
            ring_global[1] = 1 # eje z
            pinky_global[0] = 0 # eje y
            pinky_global[1] = 1 # eje z

        # -- Informacion de la mano y los dedos --
        for hand in frame.hands:
            handType = "Left hand" if hand.is_left else "Right hand"

            print "  %s, id %d, position: %s" % (handType, hand.id, hand.palm_position)

            # -- Vector normal y direccion de la mano --
            normal = hand.palm_normal
            direction = hand.direction

            # -- Hueso del brazo --
            arm = hand.arm

            # -- Informacion de los dedos --
            if len(frame.hands) == 1:
                for finger in hand.fingers:
                    print "    %s finger, id: %d, length: %fmm, width: %fmm" % (self.finger_names[finger.type],finger.id,finger.length,finger.width)
                    # -- Mueve el dedo. Usa la terminacion ";" para que arduino sepa cuando hay un cambio de grados --
                    arduino.write(str(index_grados)+';')

                    # -- Clasifica los dedos para poder usarlos luego --
                    if self.finger_names[finger.type] == "Thumb":
                        dedo = 0
                    if self.finger_names[finger.type] == "Index":
                        dedo = 1
                    if self.finger_names[finger.type] == "Middle":
                        dedo = 2
                    if self.finger_names[finger.type] == "Ring":
                        dedo = 3
                    if self.finger_names[finger.type] == "Pinky":
                        dedo = 4

                    # -- Conseguir huesos de los dedos --
                    bone = finger.bone(3) # 3 es la falange distal
                    operacion = 0 # operacion que devuelve los grados de movimiento

                    # -- Si es la primera ejecucion, se rellena la variable global --
                    if dedo == 1 and index_global[0] == 0 and index_global[1] == 1:
                        index_global[0] = bone.direction.y
                        index_global[1] = bone.direction.z
                    if dedo == 2 and middle_global[0] == 0 and middle_global[1] == 1:
                        middle_global[0] = bone.direction.y
                        middle_global[1] = bone.direction.z
                    if dedo == 3 and ring_global[0] == 0 and ring_global[1] == 1:
                        ring_global[0] = bone.direction.y
                        ring_global[1] = bone.direction.z
                    if dedo == 4 and pinky_global[0] == 0 and pinky_global[1] == 1:
                        pinky_global[0] = bone.direction.y
                        pinky_global[1] = bone.direction.z

                    # -- Se calculan los grados usando trigonometria --
                    #    Se asegura que el dedo no se retuerce hacia arriba
                    if bone.direction.y < 0:
                        operacion = 0
                    else:
                        #    Inicializar variables
                        y_aux = 0
                        z_aux = 0
                        #    Rellenar con el estado anterior para tomar la coordenada 0,0,0 como ese punto
                        #    Se evalua en funcion del dedo
                        if dedo == 1:
                            y_aux = index_global[0]
                            z_aux = index_global[1]
                        if dedo == 2:
                            y_aux = middle_global[0]
                            z_aux = middle_global[1]
                        if dedo == 3:
                            y_aux = ring_global[0]
                            z_aux = ring_global[1]
                        if dedo == 4:
                            y_aux = pinky_global[0]
                            z_aux = pinky_global[1]

                        #   Calculo de grados en funcion de las coordenadas
                        aux_1 = math.pow(bone.direction.y - abs(y_aux), 2)
                        aux_2 = math.pow(bone.direction.z - abs(z_aux), 2)
                        aux_3 = aux_1 + aux_2
                        if aux_3 > 1:
                            aux_3 = 1
                        operacion_auxiliar = math.sqrt(aux_3)
                        operacion = math.degrees(math.asin(operacion_auxiliar))




                    # -- Mostrar grados por pantalla --
                    print "      Hueso: %s, direction: %s, Movimiento: %s grados" % (self.bone_names[bone.type],bone.direction, operacion)

                    # Debug info
                    #print "      Bone: %s, start: %s, end: %s, direction: %s, Movimiento: %s grados" % (self.bone_names[bone.type],bone.prev_joint,bone.next_joint,bone.direction, operacion)

                    # Rellenar variable global con estado actual
                    #if b == 4:
                    #if dedo == 1:
                    #    index_global[0] = bone.direction.y
                    #    index_global[1] = bone.direction.z
                    #if dedo == 2:
                    #    middle_global[0] = bone.direction.y
                    #    middle_global[1] = bone.direction.z
                    #if dedo == 3:
                    #    ring_global[0] = bone.direction.y
                    #    ring_global[1] = bone.direction.z
                    #if dedo == 4:
                    #    pinky_global[0] = bone.direction.y
                    #    pinky_global[1] = bone.direction.z

                    # -- Guardar informacion de los grados de cada dedo --
                    if dedo == 1:
                        index_grados = int(operacion)
                    if dedo == 2:
                        middle_grados = int(operacion)
                    if dedo == 3:
                        ring_grados = int(operacion)
                    if dedo == 4:
                        pinky_grados = int(operacion)
                print "-------------"
                #    Reconoce la posicion de los dedos cada 0,5 segundos
                time.sleep(0.7)

    def state_string(self, state):
        if state == Leap.Gesture.STATE_START:
            return "STATE_START"

        if state == Leap.Gesture.STATE_UPDATE:
            return "STATE_UPDATE"

        if state == Leap.Gesture.STATE_STOP:
            return "STATE_STOP"

        if state == Leap.Gesture.STATE_INVALID:
            return "STATE_INVALID"

def main():
    # -- Crear un listener y un controller --
    listener = SampleListener()
    controller = Leap.Controller()

    # -- Recibe eventos del controller --
    controller.add_listener(listener)

    # -- Para el proceso cuando pulsas Enter --
    print "Press Enter to quit..."
    try:
        sys.stdin.readline()
    except KeyboardInterrupt:
        pass
    finally:
        # Remove the sample listener when done
        controller.remove_listener(listener)


if __name__ == "__main__":
    main()
