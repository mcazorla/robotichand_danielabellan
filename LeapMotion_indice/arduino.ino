#include <Servo.h>
int aux;
Servo indice; // Dedo indice
String recibido;


void setup(){
  indice.attach(9); // Usa el pin 9 del arduino
  Serial.begin(9600); // 9600 baudios
}

void loop()
{    
  if(Serial.available()){  // Si hay informacion en el serial, hacer lo siguiente
    recibido = Serial.readStringUntil(';'); // read data until newline
    aux = recibido.toInt();   // String a int       
    indice.write(aux);     // Mover servo a los grados pasados por serial
    Serial.print("Posicion: ");  
    Serial.println(recibido); // Saca por el terminal de arduino
    }
}
