#!/usr/bin/env python
import sys
import time
import cv2
from scipy import misc
import numpy as np
import signal
import sys
import os
import pickle
import time
import random
import re
import matplotlib.pyplot as plt

sys.path.insert(0, 'utils')
import multiview_dataset
import stereoht_dataset
import rendered_dataset
import utils
import resnet50
sys.path.insert(0, 'resnet50')
import test


from tensorflow.python.client import device_lib

os.environ["CUDA_VISIBLE_DEVICES"]="0"
print(device_lib.list_local_devices())

def readDataset(path):

	def natural_sort(l):
		convert = lambda text: int(text) if text.isdigit() else text.lower()
		alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
		return sorted(l, key = alphanum_key)

	import os
	from glob import glob
	imgList = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*_webcam_1*'))]
	imgList = natural_sort(imgList)

	jointList = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*_joints_transformed.txt'))]
	jointList = natural_sort(jointList)

	bboxList = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*_bbox_1*'))]
	bboxList = natural_sort(bboxList)

	return imgList, jointList, bboxList

class RHSR:

	def __init__(self):

		self.model = resnet50.ResNet50()
		filename = "resnet50/model.h5"
		self.model.load_weights(filename)
		self.model.compile(loss= 'mean_squared_error', optimizer='adam')

	def detectJoints(self, frame):
		batch = [frame]
		t = time.time()
		output = self.model.predict(np.array(batch), batch_size=1, verbose = 0)[0]
		elapsed = time.time() - t
		print "JE RunTime:", elapsed
		return output


	def runDataset(self, datasetX_test, datasetY_test, step):

		for x,y in zip(datasetX_test, datasetY_test):
			output = self.detectJoints(x) #self.model.predict(np.array(batch), batch_size=1, verbose = 0)[0]
			handContainer_inferred = test.constructHC(output)

			cv2.imshow("Image",x)
			cv2.waitKey(step)
			plt.cla()
			test.updateplot(handContainer_inferred, 'r')
			plt.pause(step/100.0) #
			cv2.waitKey(step) # '''

	def runOnLive(self):
		video_capture = cv2.VideoCapture(1)

		while True:
			ret, frame = video_capture.read()
			frame = cv2.resize(frame, (224,224))

			output = self.detectJoints(frame) #self.model.predict(np.array(batch), batch_size=1, verbose = 0)[0]
			handContainer_inferred = test.constructHC(output)

			cv2.imshow("Image",frame)
			cv2.waitKey(1)
			plt.cla()
			test.updateplot(handContainer_inferred, 'r')
			plt.pause(0.001) #
			cv2.waitKey(1) # '''




def main(args):

	test.plot()
	#pathToDataset = "/media/fran/f7f6bf6b-89c3-4c25-80a2-fdfbb2bf3381/multiview_hand_pose_dataset/data_4/"
	#datasetX_path, datasetY_path, datasetBB_path = readDataset(pathToDataset) #
	#dataset_X, dataset_Y = utils.readNextExamples(datasetX_path, datasetY_path, datasetBB_path, 0,len(datasetY_path)) #
	#dataset_X, dataset_Y = test.readNextExamplesPKL("test",0)
	ic = RHSR()
	#ic.runDataset(dataset_X, dataset_Y, 1)
	ic.runOnLive()


if __name__ == '__main__':
	main(sys.argv)
