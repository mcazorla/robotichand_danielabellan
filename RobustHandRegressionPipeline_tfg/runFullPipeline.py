
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session
config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.3
set_session(tf.Session(config=config))

import matplotlib
import matplotlib.pyplot as plt

import cv2
import numpy as np
from scipy import misc
import time
from os import listdir
from os.path import isfile, join
import re
import collections
import os
import random
import os
from glob import glob
import sys

sys.path.insert(1, 'utils')
import resnet50
import utils
from RHSR import RHSR
from HandDetectorYolo import HandDetectorYolo
sys.path.insert(0, 'resnet50')
import test


def runOnDataset():
    datasetX_path=[]
    datasetY_path=[]
    datasetBB_path=[]

    path = "data_8/"

    import os
    from glob import glob
    def natural_sort(l):
        convert = lambda text: int(text) if text.isdigit() else text.lower()
        alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
        return sorted(l, key = alphanum_key)

    imgList = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*1.jpg'))] # cambiar este 4 por el numero de camara a probar (1,2,3,4)
    imgList = imgList
    imgList = natural_sort(imgList)


    print len(imgList)



    handDetector = HandDetectorYolo()
    jointDetector = RHSR()


    for i in range(len(imgList)):

        frame = cv2.imread(imgList[i])

        toks =  imgList[i].split('/')
        number = toks[len(toks)-1].split("_")[0]
        print toks

        top_left_x,top_left_y,bottom_right_x,bottom_right_y = handDetector.extractHand(frame)
        try:
            frameCopy = frame.copy()
        except:
            continue
        cv2.rectangle(frameCopy, (top_left_x, top_left_y), (bottom_right_x, bottom_right_y), (0,255,0), 2)

        if top_left_x > 0 and top_left_y > 0 and bottom_right_x > 0 and bottom_right_y > 0:
            croppedFrame = frame[top_left_y: bottom_right_y, top_left_x: bottom_right_x]

            croppedFrame = cv2.cvtColor(croppedFrame, 4)
            croppedFrame = cv2.resize(croppedFrame,(224,224))

            output = jointDetector.detectJoints(croppedFrame)
            print output
            handContainer_inferred = test.constructHC(output)

            # create dictionary (normalized 3D hand joints)
            frame = utils.createDictAnnotations(output)

            # generate json string (leapjs compatible)
            json_str = utils.SingleFrameHandConverterNormLiveDict(frame)

            plt.cla()
            test.updateplot(handContainer_inferred, 'r')
            plt.pause(0.1) #

        cv2.imshow('Video1', frameCopy)
        if cv2.waitKey(1) & 0xFF == ord('q'): break




def runLive():

    handDetector = HandDetectorYolo()
    jointDetector = RHSR()

    visualizer = LeapJSVisualizer()
    #visualizer.show()

    video_capture = cv2.VideoCapture(1)

    n = 0
    while True:

        try:
            ret, frame = video_capture.read()
            frame = cv2.resize(frame,(640,480))

            top_left_x,top_left_y,bottom_right_x,bottom_right_y = handDetector.extractHand(frame)
            try:
                frameCopy = frame.copy()
            except:
                continue
            cv2.rectangle(frameCopy, (top_left_x, top_left_y), (bottom_right_x, bottom_right_y), (0,255,0), 2)


            if top_left_x > 0 and top_left_y > 0 and bottom_right_x > 0 and bottom_right_y > 0:
                croppedFrame = frame[top_left_y: bottom_right_y, top_left_x: bottom_right_x]

                croppedFrame = cv2.cvtColor(croppedFrame, 4)
                croppedFrame = cv2.resize(croppedFrame,(224,224))
                #croppedFrame = cv2.flip(croppedFrame, -1);

                output = jointDetector.detectJoints(croppedFrame)
                #handContainer_inferred = test.constructHC(output)


                pathToSave = "/home/fran/Escritorio/iros_video_fran/liveVideos/"
                utils.saveAnnotation(pathToSave+""+str(n)+"_prediction.txt", output)
                cv2.imwrite(pathToSave+""+str(n)+"_image.jpg", frameCopy )


                # create dictionary (normalized 3D hand joints)
                frame = utils.createDictAnnotations(output)

                # generate json string (leapjs compatible)
                json_str = utils.SingleFrameHandConverterNormLiveDict(frame)

                visualizer.refresh(json_str)

                if (n == 0):
                    visualizer.show()

                #plt.cla()
                #test.updateplot(handContainer_inferred, 'r')
                #plt.pause(0.3) #

            cv2.imshow('Video1', frameCopy)
            if cv2.waitKey(1) & 0xFF == ord('q'): break
        except:
            print "Error?"
            None

        n+= 1




if __name__ == '__main__':

    #test.plot()
    runLive()
    #runOnDataset()
