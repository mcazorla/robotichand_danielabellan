from os import listdir
from os.path import isfile, join
import re
import numpy as np
import random 
from scipy import misc
import pickle
import cv2
import time

from numpy import linalg as LA

def str_float3(vector3):
    return "["+str(round(vector3[0],4))+","+str(round(vector3[1],4))+","+str(round(vector3[2],4))+"]"

def natural_sort(l): 
    convert = lambda text: int(text) if text.isdigit() else text.lower() 
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(l, key = alphanum_key)

def writeListToTxt(list, name):
    f = open(name,'w') 
    for l in list:
        f.write(l+"\n")
    f.close

def readNextExamplesPKL(pathToPKLs, mode, start):
    datasetX = None
    datasetY = None
    #pathToPKLs = "/media/fran/f7f6bf6b-89c3-4c25-80a2-fdfbb2bf3381/batch_pkl_shtd_normal/"

    with open(pathToPKLs+mode+str(start)+"_X.pkl", 'rb') as f:
        datasetX = pickle.load(f)
    with open(pathToPKLs+mode+str(start)+"_Y.pkl", 'rb') as f:
        datasetY = pickle.load(f)

    return np.array(datasetX), np.array(datasetY)

def normalize(jointList):

    minP_ = [-238, -450, -206]
    maxP_ = [198, 365, 379]
    minP = [-1.78545, -1.60056, -0.378148]
    maxP = [0.780348, 2.17174, 2.47588]

    normalizedJointList = []

    for i in range(len(jointList)-3):
        v = None
        if i == 0 or i%3 == 0:
            v = (maxP_[0]-minP_[0]) / (maxP[0]-minP[0]) * (jointList[i]-maxP[0]) + minP[0]
        if i%3 == 1:
            v = (maxP_[1]-minP_[1]) / (maxP[1]-minP[1]) * (jointList[i]-maxP[1]) + minP[1]
        if i%3 == 2:
            v = (maxP_[2]-minP_[2]) / (maxP[0]-minP[2]) * (jointList[i]-maxP[2]) + minP[2]

        normalizedJointList.append(v)

    normalizedJointList += jointList[60:]

    return normalizedJointList

def readNextExamples(imgList, jointList, bboxList, start, incr, mode="batch"):
        
    datasetX = []
    datasetY = []
    c = 0
    for imgFile,jointFile,bboxFile in zip(imgList[start:start+incr], jointList[start:start+incr], bboxList[start:start+incr]):
        #print "Feeding ",c,"->",start,"-",start+incr,"/",len(jointList), "path", imgFile
        #print imgFile
        #print jointFile
        #print bboxFile

        bbox = [] # TOP LEFT BOTTOM RIGHT
        f = open(bboxFile, 'r')
        try:
            for l in f:
                toks = l.split()
                bbox.append(int(toks[1]))
            f.close
        except Exception as e:
            print "Error en BBox ",e
            exit(0)
            

        #print imgFile,"\n", jointFile;

        offset = 10
        try:
            imgOri = misc.imread(imgFile)
            shape = imgOri.shape
            #print shape
            if bbox[0]-offset < 0: bbox[0] = offset
            if bbox[1]-offset < 0: bbox[1] = offset
            if bbox[2]+offset > shape[0]: bbox[2] = shape[0] - offset
            if bbox[3]+offset > shape[1]: bbox[3] = shape[1] - offset #'''
            imgOri = imgOri[bbox[0]-offset:bbox[2]+offset, bbox[1]-offset:bbox[3]+offset] # - + - + #'''
            #imgOri = imgOri[bbox[0]:bbox[2], bbox[1]:bbox[3]]
            img = misc.imresize(imgOri, (224,224))

            '''cv2.imshow("W0", img)
            cv2.waitKey(0) # '''

            datasetX.append(img)
        except Exception as e:
            print "Error en Imagen ",e
            print imgFile, bbox
            print "shape ", shape[0], shape[1]
            imgOri = misc.imread(imgFile)
            cv2.imshow("W0", img)
            cv2.waitKey(0)
            exit(0)
            

        fJoints = open(jointFile, 'r') 

        jointPositionsIndex = []
        normal = []
        for line in fJoints:
            if line.split()[0] == "PALM_POSITION": # palm is always on 0,0,0 so skip it 
                continue
            if line.split()[0] == "PALM_NORMAL": # 
                Px = float(line.split()[1])
                Py = float(line.split()[2])
                Pz = float(line.split()[3])
                normal.append(Px)
                normal.append(Py)
                normal.append(Pz)
                continue

            Px = float(line.split()[1])
            Py = float(line.split()[2])
            Pz = float(line.split()[3])
 
            jointPositionsIndex.append(Px)
            jointPositionsIndex.append(Py)
            jointPositionsIndex.append(Pz)


            jointPositionsIndex.append(normal[0])
            jointPositionsIndex.append(normal[1])
            jointPositionsIndex.append(normal[2])

        c+=1

        #jointPositionsIndex = normalize(jointPositionsIndex)
 
        y = np.array(jointPositionsIndex)
        datasetY.append(y)
        #print y

    return np.array(datasetX), np.array(datasetY)


def readNextExamples_2D(imgList, jointList, bboxList, start, incr, mode="batch"):
        
    datasetX = []
    datasetY = []
    c = 0
    for imgFile,jointFile,bboxFile in zip(imgList[start:start+incr], jointList[start:start+incr], bboxList[start:start+incr]):
        #print "Feeding ",c,"->",start,"-",start+incr,"/",len(jointList), "path", imgFile
        
        print imgFile
        print jointFile
        print bboxFile

        bbox = [] # TOP LEFT BOTTOM RIGHT
        f = open(bboxFile, 'r')
        try:
            for l in f:
                toks = l.split()
                bbox.append(int(toks[1]))
            f.close
        except Exception as e:
            print "Error en BBox ",e
            exit(0)
            

        #print imgFile,"\n", jointFile;

        offset = 10
        try:
            imgOri = misc.imread(imgFile)
            shape = imgOri.shape
            #print shape
            if bbox[0]-offset < 0: bbox[0] = offset
            if bbox[1]-offset < 0: bbox[1] = offset
            if bbox[2]+offset > shape[0]: bbox[2] = shape[0] - offset
            if bbox[3]+offset > shape[1]: bbox[3] = shape[1] - offset #'''
            imgOriCrop = imgOri[bbox[0]-offset:bbox[2]+offset, bbox[1]-offset:bbox[3]+offset] # - + - + #'''
            #imgOri = imgOri[bbox[0]:bbox[2], bbox[1]:bbox[3]]
            img = misc.imresize(imgOriCrop, (224,224))

            '''cv2.imshow("W0", img)
            cv2.waitKey(0) # '''

            datasetX.append(img)
        except Exception as e:
            print "Error en Imagen ",e
            print imgFile, bbox
            print "shape ", shape[0], shape[1]
            imgOri = misc.imread(imgFile)
            cv2.imshow("W0", img)
            cv2.waitKey(0)
            exit(0)
            

        fJoints = open(jointFile, 'r') 

        jointPositionsIndex = []
        normal = []
        for line in fJoints:

            Px = float(bbox[0]-offset)-float(line.split()[1])
            Py = float(bbox[1]-offset)-float(line.split()[2])#'''
            '''Px = float(line.split()[1])
            Py = float(line.split()[2])'''
            print Px," ",Py
            jointPositionsIndex.append(Px)
            jointPositionsIndex.append(Py)

        for k in range(0,18,1):
            jointPositionsIndex.append(0)
        c+=1





        print(imgOri.shape)
        for k in range(0,len(jointPositionsIndex),2):
            print (int(jointPositionsIndex[k]),int(jointPositionsIndex[k+1]))
            cv2.circle(imgOriCrop, (int(jointPositionsIndex[k]),int(jointPositionsIndex[k+1])), 5, (255,0,0), -1)
        cv2.imshow("W0", imgOriCrop)
        cv2.waitKey(0)
        #exit(0)





 
        y = np.array(jointPositionsIndex)
        datasetY.append(y)
        #print y

    return np.array(datasetX), np.array(datasetY)

def savePKL(pathToPKLs, mode, start, datasetX, datasetY):
    
    with open(pathToPKLs+mode+str(start)+"_X.pkl", "wb") as pfile:
        pickle.dump(datasetX, pfile)
    with open(pathToPKLs+mode+str(start)+"_Y.pkl", "wb") as pfile:
        pickle.dump(datasetY, pfile)

def randomizeOrder(datasetX_path, datasetY_path, datasetBB_path):
	c = list(zip(datasetX_path, datasetY_path, datasetBB_path))
	random.shuffle(c)
	datasetX_path, datasetY_path, datasetBB_path = zip(*c)
	return datasetX_path, datasetY_path, datasetBB_path

def saveAnnotation(filename, positions):

    fOut = open(filename, "w")
    fOut.write("F4_KNU1_A " + str(positions[0]) + " " + str(positions[1]) + " " + str(positions[2]) + "\n")
    fOut.write("F4_KNU1_B " + str(positions[3]) + " " + str(positions[4]) + " " + str(positions[5]) + "\n")
    fOut.write("F4_KNU2_A " + str(positions[6]) + " " + str(positions[7]) + " " + str(positions[8]) + "\n")
    fOut.write("F4_KNU3_A " + str(positions[9]) + " " + str(positions[10]) + " " + str(positions[11]) + "\n")

    fOut.write("F3_KNU1_A " + str(positions[12]) + " " + str(positions[13]) + " " + str(positions[14]) + "\n")
    fOut.write("F3_KNU1_B " + str(positions[15]) + " " + str(positions[16]) + " " + str(positions[17]) + "\n")
    fOut.write("F3_KNU2_A " + str(positions[18]) + " " + str(positions[19]) + " " + str(positions[20]) + "\n")
    fOut.write("F3_KNU3_A " + str(positions[21]) + " " + str(positions[22]) + " " + str(positions[23]) + "\n")

    fOut.write("F1_KNU1_A " + str(positions[24]) + " " + str(positions[25]) + " " + str(positions[26]) + "\n")
    fOut.write("F1_KNU1_B " + str(positions[27]) + " " + str(positions[28]) + " " + str(positions[29]) + "\n")
    fOut.write("F1_KNU2_A " + str(positions[30]) + " " + str(positions[31]) + " " + str(positions[32]) + "\n")
    fOut.write("F1_KNU3_A " + str(positions[33]) + " " + str(positions[34]) + " " + str(positions[35]) + "\n")

    fOut.write("F2_KNU1_A " + str(positions[36]) + " " + str(positions[37]) + " " + str(positions[38]) + "\n")
    fOut.write("F2_KNU1_B " + str(positions[39]) + " " + str(positions[40]) + " " + str(positions[41]) + "\n")
    fOut.write("F2_KNU2_A " + str(positions[42]) + " " + str(positions[43]) + " " + str(positions[44]) + "\n")
    fOut.write("F2_KNU3_A " + str(positions[45]) + " " + str(positions[46]) + " " + str(positions[47]) + "\n")

    fOut.write("TH_KNU1_A " + str(positions[48]) + " " + str(positions[49]) + " " + str(positions[50]) + "\n")
    fOut.write("TH_KNU1_B " + str(positions[51]) + " " + str(positions[52]) + " " + str(positions[53]) + "\n")
    fOut.write("TH_KNU2_A " + str(positions[54]) + " " + str(positions[55]) + " " + str(positions[56]) + "\n")
    fOut.write("TH_KNU3_A " + str(positions[57]) + " " + str(positions[58]) + " " + str(positions[59]) + "\n")

    fOut.write("PALM_POSITION 0 0 0\n")
    fOut.write("PALM_NORMAL " + str(positions[60]) + " " + str(positions[61]) + " " + str(positions[62]) + "\n")

    fOut.close()

def createDictAnnotations(positions):

    frame_params = {}
    frame_params["F4_KNU1_A"] = np.asarray([-positions[0], positions[1], positions[2]], dtype=np.float)
    frame_params["F4_KNU1_B"] = np.asarray([-positions[3], positions[4], positions[5]], dtype=np.float)
    frame_params["F4_KNU2_A"] = np.asarray([-positions[6], positions[7], positions[8]], dtype=np.float)
    frame_params["F4_KNU3_A"] = np.asarray([-positions[9], positions[10], positions[11]], dtype=np.float)

    frame_params["F3_KNU1_A"] = np.asarray([-positions[12], positions[13], positions[14]], dtype=np.float)
    frame_params["F3_KNU1_B"] = np.asarray([-positions[15], positions[16], positions[17]], dtype=np.float)
    frame_params["F3_KNU2_A"] = np.asarray([-positions[18], positions[19], positions[20]], dtype=np.float)
    frame_params["F3_KNU3_A"] = np.asarray([-positions[21], positions[22], positions[23]], dtype=np.float)

    frame_params["F1_KNU1_A"] = np.asarray([-positions[24], positions[25], positions[26]], dtype=np.float)
    frame_params["F1_KNU1_B"] = np.asarray([-positions[27], positions[28], positions[29]], dtype=np.float)
    frame_params["F1_KNU2_A"] = np.asarray([-positions[30], positions[31], positions[32]], dtype=np.float)
    frame_params["F1_KNU3_A"] = np.asarray([-positions[33], positions[34], positions[35]], dtype=np.float)

    frame_params["F2_KNU1_A"] = np.asarray([-positions[36], positions[37], positions[38]], dtype=np.float)
    frame_params["F2_KNU1_B"] = np.asarray([-positions[39], positions[40], positions[41]], dtype=np.float)
    frame_params["F2_KNU2_A"] = np.asarray([-positions[42], positions[43], positions[44]], dtype=np.float)
    frame_params["F2_KNU3_A"] = np.asarray([-positions[45], positions[46], positions[47]], dtype=np.float)

    frame_params["TH_KNU1_A"] = np.asarray([-positions[48], positions[49], positions[50]], dtype=np.float)
    frame_params["TH_KNU1_B"] = np.asarray([-positions[51], positions[52], positions[53]], dtype=np.float)
    frame_params["TH_KNU2_A"] = np.asarray([-positions[54], positions[55], positions[56]], dtype=np.float)
    frame_params["TH_KNU3_A"] = np.asarray([-positions[57], positions[58], positions[59]], dtype=np.float)

    frame_params["PALM_POSITION"] = np.asarray([0.0, 0.0, 0.0], dtype=np.float)
    frame_params["PALM_NORMAL"] = np.asarray([-positions[60], positions[61], positions[62]], dtype=np.float)

    return frame_params

def SingleFrameHandConverterNormLiveDict(frame_params):

    # read joints
    #for i in range(0, 22):
    #    temp_str = frame_file.readline()
    #    temp_list = temp_str.split()
    #    frame_params[str(temp_list[0])] = np.asarray(
    #        [-float(temp_list[1]), float(temp_list[2]), float(temp_list[3])], dtype=np.float)

    # compute palm direction
    frame_params['PALM_DIRECTION'] = frame_params['F3_KNU1_B'] - frame_params['PALM_POSITION']
    frame_params['PALM_DIRECTION'] = frame_params['PALM_DIRECTION'] / LA.norm(frame_params['PALM_DIRECTION'])

    # compute fingers direction
    for i in range(1, 5):
        frame_params['F' + str(i) + '_DIRECTION'] = frame_params['F' + str(i) + '_KNU3_A'] - frame_params['F' + str(i) + '_KNU2_A']
        frame_params['F' + str(i) + '_DIRECTION'] = frame_params['F' + str(i) + '_DIRECTION'] / LA.norm(frame_params['F' + str(i) + '_DIRECTION'])

    frame_params['TH' + '_DIRECTION'] = frame_params['TH' + '_KNU3_A'] - frame_params['TH' + '_KNU2_A']
    frame_params['TH' + '_DIRECTION'] = frame_params['TH' + '_DIRECTION'] / LA.norm(frame_params['TH' + '_DIRECTION'])

    thumb_knu1_b = frame_params['TH_KNU1_B']
    frame_params['WRIST_R'] = np.asarray([float(-thumb_knu1_b[0] + 20), float(thumb_knu1_b[1]), float(thumb_knu1_b[2] + 13)], dtype=np.float)
    frame_params['WRIST'] = (frame_params['WRIST_R'] + frame_params['TH_KNU1_B'])/2

    # output json
    json_file = ""
    json_file += """ {"currentFrameRate":30.027,"devices":[],"gestures":[], \n"""
    json_file += "\"hands\":[{\"direction\":"+ str_float3(frame_params['PALM_DIRECTION']) +",\"grabStrength\":null,\"id\":2,\"palmNormal\":"+ str_float3(frame_params['PALM_NORMAL']) +",\"palmPosition\":"+ str_float3(frame_params['PALM_POSITION']) + ",\n"
    json_file += "\"pinchStrength\":null,\"stabilizedPalmPosition\":"+ str_float3(frame_params['PALM_POSITION']) +",\"type\":\"right\"}],\"id\":290,\"interactionBox\":\n"
    json_file += "{\"center\": [0, 0, 0], \"size\": [335.247, 335.247, 347.751]},\n"

    json_file += "\"pointables\": ["

    #thumb
    json_file += "{\"bases\": [[[0, 0, 0], [0, 0, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0], [0, 0, 0]],[[0, 0, 0], [0, 0, 0], [0, 0, 0]]],\n"
    json_file += "\"btipPosition\": "+ str_float3(frame_params['TH_KNU3_A']) +", \"carpPosition\": "+ str_float3(frame_params['TH_KNU1_B']) +", \"dipPosition\": "+ str_float3(frame_params['TH_KNU2_A']) +", \"direction\": "+ str_float3(frame_params['TH_DIRECTION']) +",\n"
    json_file += "\"handId\": 2, \"id\": 20, \"mcpPosition\": "+ str_float3(frame_params['TH_KNU1_B']) +", \"pipPosition\": "+ str_float3(frame_params['TH_KNU1_A']) +", \"stabilizedTipPosition\": "+str_float3(frame_params['TH_KNU3_A']) + ",\n"
    json_file += "\"tipPosition\":"+ str_float3(frame_params['TH_KNU3_A']) +",\"type\":0},\n"

    count = 1
    for i in [4, 3, 2, 1]:  # fingers
        json_file += "{\"bases\": [[[0, 0, 0], [0, 0, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0], [0, 0, 0]],[[0, 0, 0], [0, 0, 0], [0, 0, 0]]],\n"
        json_file += "\"btipPosition\": " + str_float3(frame_params['F'+str(i)+'_KNU3_A']) + ", \"carpPosition\": " + str_float3(frame_params['WRIST']) + ", \"dipPosition\": " + str_float3(frame_params['F'+str(i)+'_KNU2_A']) + ", \"direction\": " + str_float3(frame_params['F'+str(i)+'_DIRECTION']) + ",\n"
        json_file += "\"handId\": 2, \"id\": 2"+str(count)+", \"mcpPosition\": " + str_float3(frame_params['F'+str(i)+'_KNU1_B']) + ", \"pipPosition\": " + str_float3( frame_params['F'+str(i)+'_KNU1_A']) + ", \"stabilizedTipPosition\": " + str_float3(frame_params['F'+str(i)+'_KNU3_A']) + ",\n"
        json_file += "\"tipPosition\":" + str_float3(frame_params['F'+str(i)+'_KNU3_A']) + ",\"type\":"+str(count)+"}\n"

        if (i is not 1):
            json_file += ",\n"
        count += 1

    #ts = time.time()
    json_file += "], \"timestamp\": 1510130612931671}\n"
    return json_file
