import utils
import re
import collections
import random
import sys
sys.path.insert(0, 'utils')
import utils

def readDataset(path):

    import os
    from glob import glob
    imgList = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*left*.png'))]
    imgList += [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*right*.png'))]
    imgList += [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*color*.png'))]
    imgList = utils.natural_sort(imgList)

    jointList = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*left*_transformed.txt'))]
    jointList += [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*right*_transformed.txt'))]
    jointList += [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*color*_transformed.txt'))]
    jointList = utils.natural_sort(jointList)

    bboxList = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*left*_bbox.txt'))]
    bboxList += [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*right*_bbox.txt'))]
    bboxList += [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*color*_bbox.txt'))]
    bboxList = utils.natural_sort(bboxList)


    c = list(zip(imgList, jointList, bboxList))
    random.shuffle(c)
    imgList, jointList, bboxList = zip(*c)

    return imgList, jointList, bboxList