import utils
import re
import collections
import random
from scipy import misc
import cv2
import sys
sys.path.insert(0, 'utils')
import utils


def readDataset(path):

    import os
    from glob import glob
    imgList = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*.jpg'))]
    imgList = utils.natural_sort(imgList)

    jointList = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*_transformed.txt'))]
    jointList = utils.natural_sort(jointList)

    # only one joints file is provided for each frame (composed by 4 images) so we have to replicate each joint file 4 times
    jointListFinal = []
    for jointFile in jointList:
        for i in range(0,4):
            jointListFinal.append(jointFile)
    jointList = utils.natural_sort(jointListFinal)

    bboxList = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*_bbox*.txt'))]
    bboxList = utils.natural_sort(bboxList)

    c = list(zip(imgList, jointList, bboxList))
    random.shuffle(c)
    imgList, jointList, bboxList = zip(*c)

    return imgList, jointList, bboxList


def readDataset_2DGT(path):

    import os
    from glob import glob
    imgList = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*_webcam_1.jpg'))]
    imgList = utils.natural_sort(imgList)

    jointList = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*_jointsCam_1.txt'))]
    jointList = utils.natural_sort(jointList)

    bboxList = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*_bbox_1.txt'))]
    bboxList = utils.natural_sort(bboxList)

    c = list(zip(imgList, jointList, bboxList))
    random.shuffle(c)
    imgList, jointList, bboxList = zip(*c)

    return imgList, jointList, bboxList