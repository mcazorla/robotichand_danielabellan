import utils
import re
import collections
import random
import sys
sys.path.insert(0, 'utils')
import utils
import cv2

def readDataset(path):

    import os
    from glob import glob
    imgList = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*.png'))]
    imgList = imgList + imgList
    imgList = utils.natural_sort(imgList)

    jointList = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*_joints_transformed.txt'))]
    jointList = utils.natural_sort(jointList)

    bboxList = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*_bbox.txt'))]
    bboxList = utils.natural_sort(bboxList)

    imgListFil = []
    jointListFil = []
    bboxListFil = []
    # filtrar aqui aquellos BB que se salen de la imagen
    for i in range(len(bboxList)):
        #if "right" in bboxList[i] :
            bbox = [] # TOP LEFT BOTTOM RIGHT
            f = open(bboxList[i], 'r')
            try:
                for l in f:
                    toks = l.split()
                    bbox.append(int(toks[1]))
                f.close
            except Exception as e:
                print "Error en BBox ",e
                exit(0)

            if bbox[0]-10 < 0 or bbox[1]-10 <0 or bbox[2]+10 > 319 or bbox[3]+10 > 319:
                '''print bbox
                print bboxList[i]
                print imgList[i]
                print jointList[i]
                print "se sale"
                cv2.imshow("W0", cv2.imread(imgList[i]))
                cv2.waitKey(10) # '''
                continue
            imgListFil.append(imgList[i])
            jointListFil.append(jointList[i])
            bboxListFil.append(bboxList[i])

    imgList = imgListFil
    jointList = jointListFil
    bboxList = bboxListFil

    c = list(zip(imgList, jointList, bboxList))
    random.shuffle(c)
    imgList, jointList, bboxList = zip(*c)

    '''c = 0
    for (i,b) in zip(imgList,bboxList):
        if c < 20:
            img_r = cv2.imread(i)
            cv2.imshow("W0", img_r);

            bbox = [] # TOP LEFT BOTTOM RIGHT
            f = open(b, 'r')
            try:
                for l in f:
                    toks = l.split()
                    bbox.append(int(toks[1]))
                f.close
            except Exception as e:
                print "Error en BBox ",e
                exit(0)
            img_b = img_r[bbox[0]+10:bbox[2]-10, bbox[1]+10:bbox[3]-10]
            cv2.imshow("W1", img_b);
            cv2.waitKey(0); 
            c+=1#'''

    return imgList, jointList, bboxList