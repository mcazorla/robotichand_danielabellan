""" This script demonstrates the use of a convolutional LSTM network.
This network is used to predict the next frame of an artificially
generated movie which contains moving squares.
"""

from keras.utils import np_utils
from keras.callbacks import ModelCheckpoint
from keras.callbacks import TensorBoard
from keras.optimizers import SGD
from keras import backend as K 
import numpy as np
import os
from tensorflow.python.client import device_lib
from tensorflow import losses

import signal
import sys
import cv2
import time

import test

sys.path.insert(0, '../utils')
import resnet50
import multiview_dataset
import stereoht_dataset
import rendered_dataset
import utils

os.environ["CUDA_VISIBLE_DEVICES"]="0"
print device_lib.list_local_devices()


logFile = None
def signal_handler(signal, frame):
    print 'Exiting and saving log...'
    logFile.close()
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)







# read the datasets
pathToDataset_multi = "/media/fran/f7f6bf6b-89c3-4c25-80a2-fdfbb2bf3381/multiview_hand_pose_dataset/"
datasetX_path_multi, datasetY_path_multi, datasetBB_path_multi = multiview_dataset.readDataset_2DGT(pathToDataset_multi) # '''

#pathToDataset_rendered_test = "/media/fran/f7f6bf6b-89c3-4c25-80a2-fdfbb2bf3381/dataset-manos-3d/RHD/evaluation/"
#pathToDataset_rendered_train = "/media/fran/f7f6bf6b-89c3-4c25-80a2-fdfbb2bf3381/dataset-manos-3d/RHD/training/"
#datasetX_path_rendered_train, datasetY_path_rendered_train, datasetBB_path_rendered_train = rendered_dataset.readDataset(pathToDataset_rendered_train) # '''
#datasetX_path_rendered_test, datasetY_path_rendered_test, datasetBB_path_rendered_test = rendered_dataset.readDataset(pathToDataset_rendered_test) # '''


#pathToDataset_stereo = "/media/fran/f7f6bf6b-89c3-4c25-80a2-fdfbb2bf3381/stereohandtrackind_dataset/"
#datasetX_path_stereo, datasetY_path_stereo, datasetBB_path_stereo = stereoht_dataset.readDataset(pathToDataset_stereo) # '''
#datasetX_path_stereo, datasetY_path_stereo, datasetBB_path_stereo = utils.randomizeOrder(datasetX_path_stereo, datasetY_path_stereo, datasetBB_path_stereo)

# randomize
#datasetX_path_multi, datasetY_path_multi, datasetBB_path_multi = utils.randomizeOrder(datasetX_path_multi, datasetY_path_multi, datasetBB_path_multi)
#datasetX_path_stereo, datasetY_path_stereo, datasetBB_path_stereo = utils.randomizeOrder(datasetX_path_stereo, datasetY_path_stereo, datasetBB_path_stereo) # '''
#datasetX_path_rendered_train, datasetY_path_rendered_train, datasetBB_path_rendered_train = utils.randomizeOrder(datasetX_path_rendered_train, datasetY_path_rendered_train, datasetBB_path_rendered_train)



#split so the same % of each dataset falls in the splits
trainq = int(0.8 * len(datasetX_path_multi))
datasetX_path_multi_train = datasetX_path_multi[:trainq]
datasetY_path_multi_train = datasetY_path_multi[:trainq]
datasetBB_path_multi_train = datasetBB_path_multi[:trainq]
datasetX_path_multi_test = datasetX_path_multi[trainq:]
datasetY_path_multi_test = datasetY_path_multi[trainq:]
datasetBB_path_multi_test = datasetBB_path_multi[trainq:] # '''

'''trainq = int(0.8 * len(datasetX_path_stereo))
datasetX_path_stereo_train = datasetX_path_stereo[:trainq]
datasetY_path_stereo_train = datasetY_path_stereo[:trainq]
datasetBB_path_stereo_train = datasetBB_path_stereo[:trainq]
datasetX_path_stereo_test = datasetX_path_stereo[trainq:]
datasetY_path_stereo_test = datasetY_path_stereo[trainq:]
datasetBB_path_stereo_test = datasetBB_path_stereo[trainq:] # '''

# join the splits together
'''datasetX_path_train = datasetX_path_multi_train + datasetX_path_stereo_train
datasetY_path_train = datasetY_path_multi_train + datasetY_path_stereo_train
datasetBB_path_train = datasetBB_path_multi_train + datasetBB_path_stereo_train

datasetX_path_test = datasetX_path_multi_test + datasetX_path_stereo_test
datasetY_path_test = datasetY_path_multi_test + datasetY_path_stereo_test
datasetBB_path_test = datasetBB_path_multi_test + datasetBB_path_stereo_test #'''

'''datasetX_path_test = datasetX_path_stereo_test + datasetX_path_rendered_test
datasetY_path_test = datasetY_path_stereo_test + datasetY_path_rendered_test
datasetBB_path_test = datasetBB_path_stereo_test + datasetBB_path_rendered_test
datasetX_path_train = datasetX_path_stereo_train + datasetX_path_rendered_train
datasetY_path_train = datasetY_path_stereo_train + datasetY_path_rendered_train
datasetBB_path_train = datasetBB_path_stereo_train + datasetBB_path_rendered_train # '''

datasetX_path_train = datasetX_path_multi_train
datasetY_path_train = datasetY_path_multi_train
datasetBB_path_train = datasetBB_path_multi_train

datasetX_path_test = datasetX_path_multi_test
datasetY_path_test = datasetY_path_multi_test
datasetBB_path_test = datasetBB_path_multi_test

datasetX_path_train, datasetY_path_train, datasetBB_path_train = utils.randomizeOrder(datasetX_path_train, datasetY_path_train, datasetBB_path_train)



print len(datasetX_path_test) 
print len(datasetY_path_test) 
print len(datasetBB_path_test) 
print len(datasetX_path_train) 
print len(datasetY_path_train) 
print len(datasetBB_path_train) 


pathToPKLs = "/media/fran/f7f6bf6b-89c3-4c25-80a2-fdfbb2bf3381/batch_pkl_multi_normal/"
# serializing the batches
'''
examplesStart = 0 
batch_size = 70   
while examplesStart < len(datasetX_path_train):
    print "Feeding ",examplesStart,"-",examplesStart+batch_size,"/",len(datasetX_path_train)
    datasetX_train, datasetY_train = utils.readNextExamples(datasetX_path_train, datasetY_path_train, datasetBB_path_train, examplesStart, batch_size) #
    utils.savePKL(pathToPKLs, "batch", examplesStart, datasetX_train, datasetY_train)
    examplesStart += batch_size

examplesStart = 0 
batch_size = 1000
while examplesStart < len(datasetX_path_test):
    print "Feeding ",examplesStart,"-",examplesStart+batch_size,"/",len(datasetX_path_test)
    datasetX_test, datasetY_test = utils.readNextExamples(datasetX_path_test, datasetY_path_test, datasetBB_path_test, examplesStart, batch_size) #
    utils.savePKL(pathToPKLs, "test", examplesStart, datasetX_test, datasetY_test)
    examplesStart += batch_size

print "Done serializing, begin training now."
exit(0) # '''








model = resnet50.ResNet50()
filename = "/media/fran/f7f6bf6b-89c3-4c25-80a2-fdfbb2bf3381/snapshots_multi_kld/ep_16000_trloss_98.3112564087_tsloss_727.537834731.h5"
#model.load_weights(filename)




def root_mean_squared_error(y_true, y_pred):
    return K.sqrt(K.mean(K.square(y_pred - y_true), axis=-1))

def kullback_leibler_divergence(y_true, y_pred):
    y_true = K.clip(y_true, K.epsilon(), 1)
    y_pred = K.clip(y_pred, K.epsilon(), 1)
    return K.sum(y_true * K.log(y_true / y_pred), axis=-1)

'''
rvec: 
[-1.610910577026492;
  0.6874301651100789;
 -1.665671041738858]
tvec: 
 [20.63541811305343;
  81.88674949873007;
 683.7212831472322]
cameraMatrix: 
 [614.878, 0, 313.219;
  0, 615.479, 231.288;
  0, 0, 1]
distCoeffs: 
  [0.09270100000000001;
  -0.175877;
  -0.0035687;
  -0.00302299;
   0]
TMAT
  [0.02801928071315929, 0.1252727475588087, 0.9917266047794544,   20.63541811305343;
  -0.7882561666719512, -0.6073332389953229, 0.09898763819331685,  81.88674949873007;
   0.6147089844893745, -0.784508174291713,  0.08172997526932735, 683.7212831472322;
   0, 0, 0, 1]
'''

# resnet produce 60 valores
# en ypred hay 40 + 20 que son ceros
#reproyectar lo que saque la red para pasar de 3D a 2D y luego computar el error
def reproject_loss(y_true, y_pred):
    # 3d, R, t, K, dist, out_2d
    #cv::projectPoints(objectPointsTest, rvec_1, tvec_1, cameraMatrix_1, distCoeffs_1, projectedPointsTest_1);
    print y_pred
    print y_true







    

    #f = open("lossOut.txt","w")
    #f.write(str(y_pred.shape)+" "+str(y_true.shape))
    #f.close()

    ret = K.sqrt(K.mean(K.square(y_pred - y_true), axis=-1))
    #print ret.shape
    return ret





model.compile(lr=0.0001, loss = reproject_loss, optimizer='adam')    # probar logcosh

filepath="/media/fran/f7f6bf6b-89c3-4c25-80a2-fdfbb2bf3381/snapshots_shtd_normal/snap-{epoch:02d}-{loss:.10f}.hdf5"
checkpoint = ModelCheckpoint(filepath, monitor='loss', verbose=1, save_best_only=False, mode='min')

callbacks_list =[]

logFilePath = "/home/fran/Documentos/robust-hand-skeleton-regressor/RobustHandRegressionPipeline/resnet50/log_3D_3.txt"
logFile = open(logFilePath, 'w')
logFile.close()

batch_size = 70
n_iters = 10000
test_every = 1

examplesStart = 0
bestAccuracy = 0
lastTestLoss = -1


for i in range(n_iters/ test_every):
    logFile = open(logFilePath, 'a')
    print "Epoch",i
    logFile.write("Epoch " + str(i) + "\n")
    history = None
        
    for j in range(int(len(datasetX_path_train)/batch_size)):
        #print "Epoch",i,"Iteration",j,"Feeding ",examplesStart,"-",examplesStart+batch_size,"/",len(datasetX_path_train),"last test Loss", lastTestLoss
        datasetX_train, datasetY_train = utils.readNextExamples_2D(datasetX_path_train, datasetY_path_train, datasetBB_path_train, examplesStart, batch_size) #
        #datasetX_train, datasetY_train = utils.readNextExamplesPKL(pathToPKLs,"batch",examplesStart)
        
        examplesStart += batch_size
        #if datasetX_train.shape[0]==0: break
  

        history = model.fit(datasetX_train, datasetY_train, epochs=1, batch_size=batch_size, shuffle=True, callbacks=callbacks_list, verbose = 1)




        y_pred = model.predict(datasetX_train)
        reproject_loss(datasetY_train, y_pred)
        exit(0)





        lastTestLoss = history.history["loss"][0]



    print "Training Loss", history.history["loss"][0]
    logFile.write("Training Loss " + str(history.history["loss"][0]) + "\n")


    if i%test_every == 0:
        loss = 0
        #datasetX_test, datasetY_test = readNextExamples(datasetX_path_test, datasetY_path_test, datasetBB_path_test, 0, 10000 ) #len(datasetY_path_test)-1) #
        
        iterations = 0
        lossTotGlob = 0
        for j in range(0,17000,1000):
            iterations += 1
            datasetX_test, datasetY_test = utils.readNextExamplesPKL(pathToPKLs,"test",j)
            loss += model.evaluate(datasetX_test, datasetY_test, batch_size=batch_size, verbose = 0)
            #print "Test Loss",loss
            #datasetX_test, datasetY_test = readNextExamples(datasetX_path_test, datasetY_path_test, datasetBB_path_test, 10000, len(datasetY_path_test)-1) #
            #datasetX_test, datasetY_test = utils.readNextExamplesPKL(pathToPKLs,"test",10000)
            #loss2 = model.evaluate(datasetX_test, datasetY_test, batch_size=batch_size, verbose = 0)
            #loss = (loss1+loss2)/2.0



            lossTot = 0
            if i%10 == 0:
                for k in range(0,datasetX_test.shape[0]):
                    img = datasetX_test[k].copy()
                    batch = [img]
                    output = model.predict(np.array(batch), batch_size=1, verbose = 0)[0]

                    # create handContainer from the network output
                    handContainer_inferred = test.constructHC(output)
                    handContainer_actual = test.constructHC(datasetY_test[i])

                    myloss = test.computeLoss(handContainer_actual, handContainer_inferred)
                    #print "Loss: ", myloss
                    #losses.append([i, myloss])
                    lossTot += myloss/float(datasetX_test.shape[0])

                print "Mean Loss",lossTot
                lossTotGlob += lossTot

        print "My Loss en mm: ",float(lossTotGlob)/float(iterations)


        loss = float(loss)/float(iterations)
        print "Test Loss",loss
        logFile.write("Test Loss " + str(loss) + "\n")
        modelFileName = "/media/fran/f7f6bf6b-89c3-4c25-80a2-fdfbb2bf3381/snapshots_multi_kld/"+"ep_"+str(i)+"_trloss_"+str(history.history['loss'][0])+"_tsloss_"+str(loss)+".h5"
        model.save_weights(modelFileName)

    # integrar el calculo de loss como yo se
    

    logFile.close()
    
    examplesStart=0
    
if __name__ == "__main__":
    main()