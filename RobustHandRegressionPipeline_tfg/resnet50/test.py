#!/usr/bin/env python
# -*- coding: utf-8 -*-

################################################################################
# Copyright (C) 2012-2013 Leap Motion, Inc. All rights reserved.               #
# Leap Motion proprietary and confidential. Not for distribution.              #
# Use subject to the terms of the Leap Motion SDK Agreement available at       #
# https://developer.leapmotion.com/sdk_agreement, or another agreement         #
# between Leap Motion and you, your company or other organization.             #
################################################################################

import os, sys, inspect, thread, time
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import time
import random
import math
import random
from keras.utils import np_utils
from keras.callbacks import ModelCheckpoint
from keras.optimizers import SGD
import numpy as np
from scipy import misc
import re
import collections
import os
from tensorflow.python.client import device_lib
from os import listdir
from os.path import isfile, join
import cv2
import pickle
import numpy.linalg

sys.path.insert(0, '../utils')
sys.path.insert(1, '../')
sys.path.insert(2, '/home/diz/TFG_CNN/RobustHandRegressionPipeline_tfg/tools-hands')
import hands_converter
import resnet50
import multiview_dataset
import stereoht_dataset
import rendered_dataset
import utils

os.environ["CUDA_VISIBLE_DEVICES"]="0" # change to 0 if u want to run it in the gpu
print device_lib.list_local_devices()


class BoneContainer:
     def __init__(self):
        self.position = [0,0,0]
class BoneNormalContainer:
     def __init__(self):
        self.position = [0,0,0]
        self.normal = [0,0,0]

class HandContainer:
    def __init__(self):
        self.palm = BoneNormalContainer()

        self.index_metacarpal = BoneContainer()
        self.index_proximal = BoneContainer()
        self.index_intermediate = BoneContainer()
        self.index_distal = BoneContainer()

        self.middle_metacarpal = BoneContainer()
        self.middle_proximal = BoneContainer()
        self.middle_intermediate = BoneContainer()
        self.middle_distal = BoneContainer()

        self.ring_metacarpal = BoneContainer()
        self.ring_proximal = BoneContainer()
        self.ring_intermediate = BoneContainer()
        self.ring_distal = BoneContainer()

        self.pinky_metacarpal = BoneContainer()
        self.pinky_proximal = BoneContainer()
        self.pinky_intermediate = BoneContainer()
        self.pinky_distal = BoneContainer()

        self.thumb_metacarpal = BoneContainer()
        self.thumb_proximal = BoneContainer()
        self.thumb_intermediate = BoneContainer()
        self.thumb_distal = BoneContainer()



def vectorFromP1toP2(p2,p1):
    return np.array([p2[0]-p1[0], p2[1]-p1[1], p2[2]-p1[2]])


def plot():
    global ax,canvas
    fig = plt.figure()
    ax = fig.add_subplot( 111, projection="3d" )
    plt.ion()



n = 0
def updateplot(hC, c):
    global n

    try:

        palm_normal = np.array([hC.palm.normal[0], hC.palm.normal[1], hC.palm.normal[2]])

        #plt.quiver(hC.palm.position[0], hC.palm.position[1], hC.palm.position[2], hC.pinky_metacarpal.position[0], hC.pinky_metacarpal.position[1], hC.pinky_metacarpal.position[2], length=30, color=c)

        palm = np.array(hC.palm.position)

        plt.quiver(palm[0], palm[1], palm[2], palm_normal[0], palm_normal[1], palm_normal[2], length=0.1, color=c) # normal

        f1p3 = np.array(hC.index_distal.position)
        f1p2 = np.array(hC.index_intermediate.position)
        f1p1 = np.array(hC.index_proximal.position)
        f1p0 = np.array(hC.index_metacarpal.position)

        f1d0 = vectorFromP1toP2(f1p1, f1p0)
        f1d1 = vectorFromP1toP2(f1p2, f1p1)
        f1d2 = vectorFromP1toP2(f1p3, f1p2)
        f1dp = vectorFromP1toP2(palm, f1d0)

        soa = np.array([[f1p1[0],f1p1[1],f1p1[2],f1d0[0],f1d0[1],f1d0[2]],
            [f1p2[0],f1p2[1],f1p2[2],f1d1[0],f1d1[1],f1d1[2]],
            [f1p3[0],f1p3[1],f1p3[2],f1d2[0],f1d2[1],f1d2[2]]])

        X, Y, Z, U, V, W = zip(*soa)
        #plt.quiver(X, Y, Z, U, V, W, length=3, color=c)

        plt.plot([float(palm[0]), float(f1p0[0])], [float(palm[1]), float(f1p0[1])], [float(palm[2]), float(f1p0[2])], color=c)
        plt.plot([float(f1p0[0]), float(f1p1[0])], [float(f1p0[1]), float(f1p1[1])], [float(f1p0[2]), float(f1p1[2])], color=c)
        plt.plot([float(f1p1[0]), float(f1p2[0])], [float(f1p1[1]), float(f1p2[1])], [float(f1p1[2]), float(f1p2[2])], color=c)
        plt.plot([float(f1p2[0]), float(f1p3[0])], [float(f1p2[1]), float(f1p3[1])], [float(f1p2[2]), float(f1p3[2])], color=c) #'''

        f1p3 = np.array(hC.middle_distal.position)
        f1p2 = np.array(hC.middle_intermediate.position)
        f1p1 = np.array(hC.middle_proximal.position)
        f1p0 = np.array(hC.middle_metacarpal.position)

        f1d0 = vectorFromP1toP2(f1p1, f1p0)
        f1d1 = vectorFromP1toP2(f1p2, f1p1)
        f1d2 = vectorFromP1toP2(f1p3, f1p2)
        f1dp = vectorFromP1toP2(palm, f1d0)

        soa = np.array([[f1p1[0],f1p1[1],f1p1[2],f1d0[0],f1d0[1],f1d0[2]],
            [f1p2[0],f1p2[1],f1p2[2],f1d1[0],f1d1[1],f1d1[2]],
            [f1p3[0],f1p3[1],f1p3[2],f1d2[0],f1d2[1],f1d2[2]]])

        X, Y, Z, U, V, W = zip(*soa)
        #plt.quiver(X, Y, Z, U, V, W, length=3, color=c)

        plt.plot([float(palm[0]), float(f1p0[0])], [float(palm[1]), float(f1p0[1])], [float(palm[2]), float(f1p0[2])], color=c)
        plt.plot([float(f1p0[0]), float(f1p1[0])], [float(f1p0[1]), float(f1p1[1])], [float(f1p0[2]), float(f1p1[2])], color=c)
        plt.plot([float(f1p1[0]), float(f1p2[0])], [float(f1p1[1]), float(f1p2[1])], [float(f1p1[2]), float(f1p2[2])], color=c)
        plt.plot([float(f1p2[0]), float(f1p3[0])], [float(f1p2[1]), float(f1p3[1])], [float(f1p2[2]), float(f1p3[2])], color=c)


        f1p3 = np.array(hC.ring_distal.position)
        f1p2 = np.array(hC.ring_intermediate.position)
        f1p1 = np.array(hC.ring_proximal.position)
        f1p0 = np.array(hC.ring_metacarpal.position)

        f1d0 = vectorFromP1toP2(f1p1, f1p0)
        f1d1 = vectorFromP1toP2(f1p2, f1p1)
        f1d2 = vectorFromP1toP2(f1p3, f1p2)
        f1dp = vectorFromP1toP2(palm, f1d0)

        soa = np.array([[f1p1[0],f1p1[1],f1p1[2],f1d0[0],f1d0[1],f1d0[2]],
            [f1p2[0],f1p2[1],f1p2[2],f1d1[0],f1d1[1],f1d1[2]],
            [f1p3[0],f1p3[1],f1p3[2],f1d2[0],f1d2[1],f1d2[2]]])

        X, Y, Z, U, V, W = zip(*soa)
        #plt.quiver(X, Y, Z, U, V, W, length=3, color=c)

        plt.plot([float(palm[0]), float(f1p0[0])], [float(palm[1]), float(f1p0[1])], [float(palm[2]), float(f1p0[2])], color=c)
        plt.plot([float(f1p0[0]), float(f1p1[0])], [float(f1p0[1]), float(f1p1[1])], [float(f1p0[2]), float(f1p1[2])], color=c)
        plt.plot([float(f1p1[0]), float(f1p2[0])], [float(f1p1[1]), float(f1p2[1])], [float(f1p1[2]), float(f1p2[2])], color=c)
        plt.plot([float(f1p2[0]), float(f1p3[0])], [float(f1p2[1]), float(f1p3[1])], [float(f1p2[2]), float(f1p3[2])], color=c)


        f1p3 = np.array(hC.pinky_distal.position)
        f1p2 = np.array(hC.pinky_intermediate.position)
        f1p1 = np.array(hC.pinky_proximal.position)
        f1p0 = np.array(hC.pinky_metacarpal.position)

        f1d0 = vectorFromP1toP2(f1p1, f1p0)
        f1d1 = vectorFromP1toP2(f1p2, f1p1)
        f1d2 = vectorFromP1toP2(f1p3, f1p2)
        f1dp = vectorFromP1toP2(palm, f1d0)

        soa = np.array([[f1p1[0],f1p1[1],f1p1[2],f1d0[0],f1d0[1],f1d0[2]],
            [f1p2[0],f1p2[1],f1p2[2],f1d1[0],f1d1[1],f1d1[2]],
            [f1p3[0],f1p3[1],f1p3[2],f1d2[0],f1d2[1],f1d2[2]]])

        X, Y, Z, U, V, W = zip(*soa)
        #plt.quiver(X, Y, Z, U, V, W, length=3, color=c)

        plt.plot([float(palm[0]), float(f1p0[0])], [float(palm[1]), float(f1p0[1])], [float(palm[2]), float(f1p0[2])], color=c)
        plt.plot([float(f1p0[0]), float(f1p1[0])], [float(f1p0[1]), float(f1p1[1])], [float(f1p0[2]), float(f1p1[2])], color=c)
        plt.plot([float(f1p1[0]), float(f1p2[0])], [float(f1p1[1]), float(f1p2[1])], [float(f1p1[2]), float(f1p2[2])], color=c)
        plt.plot([float(f1p2[0]), float(f1p3[0])], [float(f1p2[1]), float(f1p3[1])], [float(f1p2[2]), float(f1p3[2])], color=c)

        f1p3 = np.array(hC.thumb_distal.position)
        f1p2 = np.array(hC.thumb_intermediate.position)
        f1p1 = np.array(hC.thumb_proximal.position)
        f1p0 = np.array(hC.thumb_metacarpal.position)

        f1d0 = vectorFromP1toP2(f1p1, f1p0)
        f1d1 = vectorFromP1toP2(f1p2, f1p1)
        f1d2 = vectorFromP1toP2(f1p3, f1p2)
        f1dp = vectorFromP1toP2(palm, f1d0)

        soa = np.array([[f1p1[0],f1p1[1],f1p1[2],f1d0[0],f1d0[1],f1d0[2]],
            [f1p2[0],f1p2[1],f1p2[2],f1d1[0],f1d1[1],f1d1[2]],
            [f1p3[0],f1p3[1],f1p3[2],f1d2[0],f1d2[1],f1d2[2]]])

        X, Y, Z, U, V, W = zip(*soa)
        #plt.quiver(X, Y, Z, U, V, W, length=3, color=c)

        plt.plot([float(palm[0]), float(f1p0[0])], [float(palm[1]), float(f1p0[1])], [float(palm[2]), float(f1p0[2])], color=c)
        plt.plot([float(f1p0[0]), float(f1p1[0])], [float(f1p0[1]), float(f1p1[1])], [float(f1p0[2]), float(f1p1[2])], color=c)
        plt.plot([float(f1p1[0]), float(f1p2[0])], [float(f1p1[1]), float(f1p2[1])], [float(f1p1[2]), float(f1p2[2])], color=c)
        plt.plot([float(f1p2[0]), float(f1p3[0])], [float(f1p2[1]), float(f1p3[1])], [float(f1p2[2]), float(f1p3[2])], color=c) #'''


        plt.plot([0,0],[0,0],[0,30])
        plt.plot([0,0],[0,30],[0,0])
        plt.plot([0,30],[0,0],[0,0])
        ax.set_zlim3d(-10, 120)
        ax.set_ylim3d(-120, 120)
        ax.set_xlim3d(-120, 120)
        plt.show()

    except Exception as e:
        print type(e)     # the exception instance
        print e.args      # arguments stored in .args
        print e           # __str__ allows args to be printed directly
        #handContainer = HandContainer()
        None

    n = n+1

def constructHC(output):
    handContainer = HandContainer()
    handContainer.index_proximal.position = [output[0], output[1], output[2]]
    handContainer.index_metacarpal.position = [output[3], output[4], output[5]]
    handContainer.index_intermediate.position = [output[6], output[7], output[8]]
    handContainer.index_distal.position = [output[9], output[10], output[11]]

    handContainer.middle_proximal.position = [output[12], output[13], output[14]]
    handContainer.middle_metacarpal.position = [output[15], output[16], output[17]]
    handContainer.middle_intermediate.position = [output[18], output[19], output[20]]
    handContainer.middle_distal.position = [output[21], output[22], output[23]]

    handContainer.pinky_proximal.position = [output[24], output[25], output[26]]
    handContainer.pinky_metacarpal.position = [output[27], output[28], output[29]]
    handContainer.pinky_intermediate.position = [output[30], output[31], output[32]]
    handContainer.pinky_distal.position = [output[33], output[34], output[35]]

    handContainer.ring_proximal.position = [output[36], output[37], output[38]]
    handContainer.ring_metacarpal.position = [output[39], output[40], output[41]]
    handContainer.ring_intermediate.position = [output[42], output[43], output[44]]
    handContainer.ring_distal.position = [output[45], output[46], output[47]]

    handContainer.thumb_proximal.position = [output[48], output[49], output[50]]
    handContainer.thumb_metacarpal.position = [output[51], output[52], output[53]]
    handContainer.thumb_intermediate.position = [output[54], output[55], output[56]]
    handContainer.thumb_distal.position  = [output[57], output[58], output[59]]

    handContainer.palm.position = [0,0,0]
    handContainer.palm.normal = [output[60], output[61], output[62]]

    return handContainer

def euclideanDistance(v1, v2):
    return numpy.linalg.norm(np.array(v1)-np.array(v2))

def computeLoss(actualHandC, inferredHandC):
    loss_metacarpal = 0
    loss_proximal = 0
    loss_intermediate = 0
    loss_distal = 0

    loss_metacarpal += euclideanDistance(np.array(actualHandC.index_metacarpal.position), np.array(inferredHandC.index_metacarpal.position))
    loss_proximal += euclideanDistance(np.array(actualHandC.index_proximal.position), np.array(inferredHandC.index_proximal.position))
    loss_intermediate += euclideanDistance(np.array(actualHandC.index_intermediate.position), np.array(inferredHandC.index_intermediate.position))
    loss_distal += euclideanDistance(np.array(actualHandC.index_distal.position), np.array(inferredHandC.index_distal.position))

    loss_metacarpal += euclideanDistance(np.array(actualHandC.middle_metacarpal.position), np.array(inferredHandC.middle_metacarpal.position))
    loss_proximal += euclideanDistance(np.array(actualHandC.middle_proximal.position), np.array(inferredHandC.middle_proximal.position))
    loss_intermediate += euclideanDistance(np.array(actualHandC.middle_intermediate.position), np.array(inferredHandC.middle_intermediate.position))
    loss_distal += euclideanDistance(np.array(actualHandC.middle_distal.position), np.array(inferredHandC.middle_distal.position))

    loss_metacarpal += euclideanDistance(np.array(actualHandC.ring_metacarpal.position), np.array(inferredHandC.ring_metacarpal.position))
    loss_proximal += euclideanDistance(np.array(actualHandC.ring_proximal.position), np.array(inferredHandC.ring_proximal.position))
    loss_intermediate += euclideanDistance(np.array(actualHandC.ring_intermediate.position), np.array(inferredHandC.ring_intermediate.position))
    loss_distal += euclideanDistance(np.array(actualHandC.ring_distal.position), np.array(inferredHandC.ring_distal.position))

    loss_metacarpal += euclideanDistance(np.array(actualHandC.pinky_metacarpal.position), np.array(inferredHandC.pinky_metacarpal.position))
    loss_proximal += euclideanDistance(np.array(actualHandC.pinky_proximal.position), np.array(inferredHandC.pinky_proximal.position))
    loss_intermediate += euclideanDistance(np.array(actualHandC.pinky_intermediate.position), np.array(inferredHandC.pinky_intermediate.position))
    loss_distal += euclideanDistance(np.array(actualHandC.pinky_distal.position), np.array(inferredHandC.pinky_distal.position))

    loss_metacarpal += euclideanDistance(np.array(actualHandC.thumb_metacarpal.position), np.array(inferredHandC.thumb_metacarpal.position))
    loss_proximal += euclideanDistance(np.array(actualHandC.thumb_proximal.position), np.array(inferredHandC.thumb_proximal.position))
    loss_intermediate += euclideanDistance(np.array(actualHandC.thumb_intermediate.position), np.array(inferredHandC.thumb_intermediate.position))
    loss_distal += euclideanDistance(np.array(actualHandC.thumb_distal.position), np.array(inferredHandC.thumb_distal.position))

    normal = euclideanDistance(np.array(actualHandC.palm.normal), np.array(inferredHandC.palm.normal))
    # Falta incluir el loss de la normal

    loss = loss_metacarpal + loss_proximal + loss_intermediate + loss_distal + normal

    '''print "Meta", loss_metacarpal
    print "Prox", loss_proximal
    print "Inte", loss_intermediate
    print "Dist", loss_distal'''

    return (loss/float(21))

def main():

    plot()

    datasetX_path=[]
    datasetY_path=[]


    pathToPKLs = "/media/fran/f7f6bf6b-89c3-4c25-80a2-fdfbb2bf3381/batch_pkl_shtd_normal/"
    datasetX_test, datasetY_test = utils.readNextExamplesPKL(pathToPKLs,"test",0)



    #datasetX_test, datasetY_test = readNextExamplesPKL("test",10001)
    #print datasetX_test.shape, datasetY_test.shape

    model = resnet50.ResNet50()
    filename = "/home/fran/Documentos/robust-hand-skeleton-regressor/experiments/exp6/ep_779_trloss_0.680937111378_tsloss_4.84591038799.h5"
    model.load_weights(filename)
    model.compile(lr=0.0001, loss= 'mean_squared_error', optimizer='adam')

    losses = []

    lossTot = 0
    for i in range(0,datasetX_test.shape[0]):
        img = datasetX_test[i].copy()
        batch = [img]
        output = model.predict(np.array(batch), batch_size=1, verbose = 0)[0]

        # create handContainer from the network output
        handContainer_inferred = constructHC(output)
        handContainer_actual = constructHC(datasetY_test[i])

        loss = computeLoss(handContainer_actual, handContainer_inferred)
        print "Loss: ", loss
        losses.append([i, loss])
        lossTot += loss/float(datasetX_test.shape[0])

        '''cv2.imshow("Sample Y",img)
        cv2.waitKey(2)
        plt.cla()
        updateplot(handContainer_inferred, 'g')
        updateplot(handContainer_actual, 'r')
        plt.pause(5)
        cv2.waitKey(100) # '''
        #cv2.destroyAllWindows()
        #if i > 20: break
    print "Mean Loss",lossTot



    losses.sort(key=lambda x: x[1])
    #print losses

    print "waiting"
    raw_input("Press something to continue...")

    j = 0
    for i in range(len(losses)): #range((len(losses)/2)-40,(len(losses)/2)+40 ):
        i = len(losses)-1-i-10
        print i
        row = losses[i]
        print row[0]
        img = datasetX_test[row[0]].copy()
        batch = [img]
        output = model.predict(np.array(batch), batch_size=1, verbose = 0)[0]

        # create handContainer from the network output
        handContainer_inferred = constructHC(output)
        handContainer_actual = constructHC(datasetY_test[row[0]])

        err = computeLoss(handContainer_actual, handContainer_inferred)
        print "Loss: ", err


        utils.saveAnnotation("/home/fran/Documentos/robust-hand-skeleton-regressor/RobustHandRegressionPipeline/resnet50/results/worst_stereo/"+str(err)+"_"+str(i)+"_inferred", output)
        utils.saveAnnotation("/home/fran/Documentos/robust-hand-skeleton-regressor/RobustHandRegressionPipeline/resnet50/results/worst_stereo/"+str(err)+"_"+str(i)+"_gt", datasetY_test[row[0]])
        imgRGB = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
        cv2.imwrite("/home/fran/Documentos/robust-hand-skeleton-regressor/RobustHandRegressionPipeline/resnet50/results/worst_stereo/"+str(err)+"_"+str(i)+".jpg", imgRGB)

        cv2.imshow("Sample Y",img)
        cv2.waitKey(1)
        plt.cla()
        updateplot(handContainer_inferred, 'g')
        updateplot(handContainer_actual, 'r')
        plt.pause(5) #
        cv2.waitKey(0)
        #cv2.destroyAllWindows()


        '''j+=1
        if j > 10: break; # '''






    '''lossTot2 = 0

    datasetX_test, datasetY_test = utils.readNextExamplesPKL(pathToPKLs,"test",10000)
    for k in range(0,datasetX_test.shape[0]):
        img = datasetX_test[k].copy()
        batch = [img]
        output = model.predict(np.array(batch), batch_size=1, verbose = 0)[0]

        # create handContainer from the network output
        handContainer_inferred = constructHC(output)
        handContainer_actual = constructHC(datasetY_test[k])

        loss = computeLoss(handContainer_actual, handContainer_inferred)
        print "Loss: ", loss
        losses.append([i+k, loss])
        lossTot2 += loss/float(datasetX_test.shape[0])

        cv2.imshow("Sample Y",img)
        cv2.waitKey(1)
        plt.cla()
        updateplot(handContainer_inferred, 'g')
        updateplot(handContainer_actual, 'r')
        plt.pause(0.1)
        #cv2.destroyAllWindows()
    print "Mean Loss",str((lossTot+lossTot2)/2.0) # '''

    losses.sort(key=lambda x: x[1])

    quart = [0]*10
    for i in range(len(losses)):
        #print "Considering loss", losses[i][1]
        if losses[i][1]>= 0 and losses[i][1]< 5:
            quart[0] += 1
        if losses[i][1]>= 5 and losses[i][1]< 10:
            quart[1] += 1
        if losses[i][1]>= 10 and losses[i][1]< 15:
            quart[2] += 1
        if losses[i][1]>= 15 and losses[i][1]< 20:
            quart[3] += 1
        if losses[i][1]>= 20 and losses[i][1]< 25:
            quart[4] += 1
        if losses[i][1]>= 25 and losses[i][1]< 30:
            quart[5] += 1
        if losses[i][1]>= 30 and losses[i][1]< 35:
            quart[6] += 1
        if losses[i][1]>= 35 and losses[i][1]< 40:
            quart[7] += 1
        if losses[i][1]>= 40 and losses[i][1]< 45:
            quart[8] += 1
        if losses[i][1]>= 45:
            quart[9] += 1

    for i in range(len(quart)):
        print "->", quart[i], (quart[i]/float(len(losses)))

    print "# samples", len(losses)


def mainSingleFile():
    plot()
    visualizer = LeapJSVisualizer()

    imFile = "/home/fran/Imágenes/samples_topaper/7.92903209547_8875.jpg"
    gtFile = "/home/fran/Imágenes/samples_topaper/7.92903209547_8875_gt"
    hxFile = "/home/fran/Imágenes/samples_topaper/7.92903209547_8875_inferred"

    hands_converter.SingleFrameHandConverterNorm(hxFile)

    img = cv2.imread(imFile)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    y =  utils.getArrayFromJointFile(gtFile)
    hx = utils.getArrayFromJointFile(hxFile)

    #print y
    #print hx

    handContainer_inferred = constructHC(hx)
    handContainer_actual = constructHC(y)

    visualizer.showJSON(hxFile+".json")

    cv2.imshow("Sample Y",img)
    cv2.waitKey(100)
    plt.cla()
    updateplot(handContainer_inferred, 'g')
    updateplot(handContainer_actual, 'r')
    plt.pause(50)
    cv2.waitKey(0) # '''
    #cv2.destroyAllWindows()

    visualizer.kill()







if __name__ == "__main__":
    main()
    #mainSingleFile()
