#!/usr/bin/env python

import os.path as osp
import sys
from ctypes import *
import math
import random
import time
import matplotlib.pyplot as plt
import numpy as np
import scipy.io as sio
import os, sys, cv2
import argparse
import os
import time

def sample(probs):
    s = sum(probs)
    probs = [a/s for a in probs]
    r = random.uniform(0, 1)
    for i in range(len(probs)):
        r = r - probs[i]
        if r <= 0:
            return i
    return len(probs)-1

def c_array(ctype, values):
    return (ctype * len(values))(*values)

class BOX(Structure):
    _fields_ = [("x", c_float),
                ("y", c_float),
                ("w", c_float),
                ("h", c_float)]

class IMAGE(Structure):
    _fields_ = [("w", c_int),
                ("h", c_int),
                ("c", c_int),
                ("data", POINTER(c_float))]

class METADATA(Structure):
    _fields_ = [("classes", c_int),
                ("names", POINTER(c_char_p))]


class HandDetectorYolo:

    def __init__(self):

        self.make_boxes = None
        self.num_boxes = None
        self.make_probs = None
        self.network_detect = None
        self.setect = None
        self.load_image = None
        self.free_ptrs = None
        self.free_image = None
        self.meta = None
        self.ndarray_image = None
        self.net = None

        self.thresh=.5
        self.hier_thresh=.5
        self.nms=.45

        self.setup()

    def nparray_to_image(self, img):

        data = img.ctypes.data_as(POINTER(c_ubyte))
        image = self.ndarray_image(data, img.ctypes.shape, img.ctypes.strides)

        return image

    def setup(self):
        lib = CDLL("yolo/libdarknet.so", RTLD_GLOBAL)
        lib.network_width.argtypes = [c_void_p]
        lib.network_width.restype = c_int
        lib.network_height.argtypes = [c_void_p]
        lib.network_height.restype = c_int

        predict = lib.network_predict
        predict.argtypes = [c_void_p, POINTER(c_float)]
        predict.restype = POINTER(c_float)

        self.make_boxes = lib.make_boxes
        self.make_boxes.argtypes = [c_void_p]
        self.make_boxes.restype = POINTER(BOX)

        self.free_ptrs = lib.free_ptrs
        self.free_ptrs.argtypes = [POINTER(c_void_p), c_int]

        self.num_boxes = lib.num_boxes
        self.num_boxes.argtypes = [c_void_p]
        self.num_boxes.restype = c_int

        self.make_probs = lib.make_probs
        self.make_probs.argtypes = [c_void_p]
        self.make_probs.restype = POINTER(POINTER(c_float))

        self.detect = lib.network_predict
        self.detect.argtypes = [c_void_p, IMAGE, c_float, c_float, c_float, POINTER(BOX), POINTER(POINTER(c_float))]

        reset_rnn = lib.reset_rnn
        reset_rnn.argtypes = [c_void_p]

        load_net = lib.load_network
        load_net.argtypes = [c_char_p, c_char_p, c_int]
        load_net.restype = c_void_p

        self.free_image = lib.free_image
        self.free_image.argtypes = [IMAGE]

        letterbox_image = lib.letterbox_image
        letterbox_image.argtypes = [IMAGE, c_int, c_int]
        letterbox_image.restype = IMAGE

        load_meta = lib.get_metadata
        lib.get_metadata.argtypes = [c_char_p]
        lib.get_metadata.restype = METADATA

        self.load_image = lib.load_image_color
        self.load_image.argtypes = [c_char_p, c_int, c_int]
        self.load_image.restype = IMAGE

        self.ndarray_image = lib.ndarray_to_image
        self.ndarray_image.argtypes = [POINTER(c_ubyte), POINTER(c_long), POINTER(c_long)]
        self.ndarray_image.restype = IMAGE

        predict_image = lib.network_predict_image
        predict_image.argtypes = [c_void_p, IMAGE]
        predict_image.restype = POINTER(c_float)

        self.network_detect = lib.network_detect
        self.network_detect.argtypes = [c_void_p, IMAGE, c_float, c_float, c_float, POINTER(BOX), POINTER(POINTER(c_float))]

        self.net = load_net("yolo/yolo-hands.cfg", "yolo/yolo-hands.weights", 0)
        self.meta = load_meta("yolo/hands.data")


    def extractHand(self, frame):

        im = self.nparray_to_image(frame)

        boxes = self.make_boxes(self.net)
        probs = self.make_probs(self.net)
        num =   self.num_boxes(self.net)

        start_time = time.time()
        self.network_detect(self.net, im, self.thresh, self.hier_thresh, self.nms, boxes, probs)
        print("%f seconds" % (time.time() - start_time))

        res = []
        for j in range(num):
            for i in range(self.meta.classes):
                if probs[j][i] > 0:
                    res.append((self.meta.names[i], probs[j][i], (boxes[j].x, boxes[j].y, boxes[j].w, boxes[j].h)))
        res = sorted(res, key=lambda x: -x[1])
        self.free_image(im)
        self.free_ptrs(cast(probs, POINTER(c_void_p)), num)

        #print res

        if len(res) > 0:
            return int(int(res[0][2][0])-int(res[0][2][2])/2), int(int(res[0][2][1])-int(res[0][2][3])/2), int(int(res[0][2][0]+res[0][2][2]/2)), int(int(res[0][2][1]+res[0][2][3]/2))
        else:
            return 0,0,0,0


    def runOnLive(self, cam):

        video_capture = cv2.VideoCapture(cam)

        while True:
            ret, frame = video_capture.read()
            top_left_x,top_left_y,bottom_right_x,bottom_right_y = self.extractHand(frame)

            #if top_left_y < 0 or bottom_right_y < 0 or top_left_x < 0 or bottom_right_x < 0: continue
            cv2.rectangle(frame, (top_left_x, top_left_y), (bottom_right_x, bottom_right_y), (0,255,0), 2) # '''
            cv2.imshow('Video', frame)
            if cv2.waitKey(1) & 0xFF == ord('q'): break




def main(args):
    hd = HandDetectorYolo()
    hd.runOnLive(1)



if __name__ == '__main__':
    main(sys.argv)
