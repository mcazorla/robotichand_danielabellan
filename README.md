# Bienvenido a mi trabajo de final de grado
## Introducción
Hola, soy Daniel Abellán Sánchez y este es el repositorio de mi trabajo de final de grado de Ingeniería Robótica en la Universidad de  Alicante.  
Iré subiendo mi trabajo a medida que avance en él en esta página.  
Puedes consultar mi progreso en la [Wiki](https://bitbucket.org/mcazorla/robotichand_danielabellan/wiki/Home) de este repositorio de Bitbucket.

## Archivos
Iré subiendo los archivos a medida que actualice la Wiki. Por el momento solo está disponible el código del proyecto de Leap Motion (10 de octubre de 2019).
Para poder descargar los archivos ejecuta:
```sh
$ git clone https://bitbucket.org/mcazorla/robotichand_danielabellan.git
```